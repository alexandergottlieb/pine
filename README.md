# Pine
An Intuitive Web Interface for Treebanking

## Get Started

Head to the [Docs](https://docs.pine.alexandergottlieb.com/) to get up and running.

# Acknowledgements
- [Fontawesome](https://fontawesome.com/license)
